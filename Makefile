all: clean deno_lint deno_docker tgbot app

deno_lint:
	cd cli && deno lint

deno: ./kollagen

./kollagen:
	cd cli && deno compile --import-map ./import_map.json --allow-read --allow-write --unstable -o ../kollagen main.ts

deno_docker:
	docker run --rm -v $$PWD:/app denoland/deno compile --import-map /app/cli/import_map.json --allow-read --allow-write --unstable -o /app/kollagen /app/cli/main.ts

app:
	npm ci --cache .npm --prefer-offline && npm run build

tgbot: ./kollagen
	docker build -t kollagen-bot -f tgbot/Dockerfile .

clean:
	rm -rvf dist kollagen