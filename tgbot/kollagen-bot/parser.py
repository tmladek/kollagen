import argparse
import re


class ArgumentParserError(RuntimeError):
    pass


class SafeArgumentParser(argparse.ArgumentParser):
    def error(self, message):
        raise ArgumentParserError(message)

def safe_str(val: str):
    if re.findall(r'[^\w,]', val):
        raise RuntimeError("No special characters in arguments allowed!")
    return val