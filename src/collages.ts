import { CollageModes } from "./common/collages";

export default class BrowserCollageModes extends CollageModes<CanvasRenderingContext2D, ImageBitmap, any> {
    drawImage(ctx: CanvasRenderingContext2D, image: ImageBitmap, sx: number, sy: number, sw: number, sh: number, dx: number, dy: number, dw: number, dh: number): void {
        ctx.drawImage(image, sx, sy, sw, sh, dx, dy, dw, dh);
    }

    createCanvas(w: number, h: number) {
        throw new Error("Method not implemented.");
    }
    canvasToImage(canvas: any): PromiseLike<ImageBitmap> {
        throw new Error("Method not implemented.");
    }
}