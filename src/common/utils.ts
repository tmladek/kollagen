/**
 * Shuffles array in place.
 * @param {Array} a items An array containing the items.
 */
export function shuffle<T>(a: T[]): T[] {
    let j, x, i;
    let b = Array.from(a);
    for (i = b.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = b[i];
        b[i] = b[j];
        b[j] = x;
    }
    return b;
}

export function randint(n: number) {
    return Math.floor(Math.random() * n);
}

export function choice<T>(arr: readonly T[]): T {
    return arr[randint(arr.length)];
}

export function range(n: number): number[] {
    return Array.from({length: n}, (x, i) => i);
}
