export interface CollageMode<C extends CollageContext, I extends CollageImage> {
    name: string;
    minImages: number;
    getSegments: (ctx: C, config?: CollageConfig, images?: I[]) => Segment[];
    place: (ctx: C, images: I[], segments: Segment[]) => void;
    forceConfig?: CollageConfig;
}

export interface CollageConfig {
    numImages?: number;
}

export interface Segment {
    x: number;
    y: number;
    w: number;
    h: number;
}


export interface CollageContext {
    globalCompositeOperation: string;
    canvas: CollageCanvas
}

export interface CollageCanvas {
    width: number;
    height: number;
    getContext: (x: '2d') => CollageContext | null
}

export interface CollageImage {
    width: number;
    height: number;
}