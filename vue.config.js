const packageJSON = require('./package.json');
process.env.VUE_APP_VERSION = packageJSON.version;
process.env.VUE_APP_HOMEPAGE = packageJSON.homepage;

module.exports = {
    publicPath: '/tools/kollagen'
};
