import { CollageModeType, DisplayCollageModeType, isCollageModeType, isDisplayCollageModeType } from "../src/common/collages.ts";

export function parseDisplayCollageModes(input: string): DisplayCollageModeType[] {
    const result: DisplayCollageModeType[] = [];

    input.split(",").map((m) => m.trim()).forEach((m) => {
        if (isDisplayCollageModeType(m)) {
            result.push(m);
        } else {
            throw Error(`"${m}" is not a valid collage mode.`);
        }
    });

    return result;
}

export function parseCollageModes(input: string): CollageModeType[] {
    const result: CollageModeType[] = [];

    input.split(",").map((m) => m.trim()).forEach((m) => {
        if (isCollageModeType(m)) {
            result.push(m);
        } else {
            throw Error(`"${m}" is not a valid collage mode.`);
        }
    });

    return result;
}