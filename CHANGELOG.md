# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.0] - 2020-07-20
### Added
- Specific modes can now be excluded from rotation in shuffle & recursive modes 

## [0.1.0] - 2020-07-17
### Added
- Recursive collage placement mode! 

### Changed
- Refactored modes - they are no longer solely responsible for drawing, but can use common rendering functions through composition.

### Fixed
- Incorrect size calculation for "Irregular column" mode.

## [0.0.4] - 2020-07-17 
### Fixed
- Blending mode now properly resets after using the "Blend" collage mode. 

## [0.0.3] - 2020-07-17
### Added
- "Blend" collage mode - based on a random selection of compositing operations.
- LICENSE file.

## [0.0.2] - 2020-07-16
### Changed
- Split "Grid" mode into "Clean grid" and "Irregular grid" (because "clean cropping" config only really applied to this mode.)

## [0.0.1] - 2020-07-15
### Added
- First public version!
- Basic modes: Grid, (ir)regular row, (ir)regular column,  concentric modes.


[Unreleased]: https://gitlab.com/tmladek/kollagen/-/compare/v0.2.0...master
[0.2.0]: https://gitlab.com/tmladek/kollagen/-/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/tmladek/kollagen/-/compare/v0.0.4...v0.1.0
[0.0.4]: https://gitlab.com/tmladek/kollagen/-/compare/v0.0.3...v0.0.4
[0.0.3]: https://gitlab.com/tmladek/kollagen/-/compare/v0.0.2...v0.0.3
[0.0.2]: https://gitlab.com/tmladek/kollagen/-/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/tmladek/kollagen/-/tags/v0.0.1

